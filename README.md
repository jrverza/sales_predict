<div align="center"> <h1 align="center"> Sales Predict Project</h1> </div>

## About the project
This project aims to develop an End-to-End Data Science project, applying Machine Learning models to predict sales for a store.

### Business Context
Rossmann operates over 3,000 drug stores in 7 European countries. Currently, Rossmann store managers are tasked with predicting their daily sales for up to six weeks in advance. Store sales are influenced by many factors, including promotions, competition, school and state holidays, seasonality, and locality. The CFO plans store renovations, but the price that will be spent on renovations needs to be within the budget of each store, for that he needs know the sales forecast value in the next 6 weeks.

### Business Issue
The CFO want to know:

    - What is the sales forecast for each store in 6 weeks?

### Features
The data for developing this project can be found in [link](https://www.kaggle.com/c/rossmann-store-sales/data). The csv files are divided in train and test. All features description can be found at the same link before.

### Business Premises
Some assumptions were made with a quick overview of the data:

### Solution Planning
The solution was planned following the methodology CRISP-DM applied in DS.

<img src="img/CRISP-DM.png" alt="drawing" width="500" align="center"/>

The tools used were Python 3.8, Jupyter Notebook, Jupyter Lab, Sublime Text, Heroku, Git and Telegram. All the libraries needed for this project are in the requiments.txt

Solution steps:

    - Business understanding
    - Data Collection: Kaggle
    - Data description: check dimensions, check NA, descriptive statistical
    - Feature Engineering: mind map creation, hypoyheses creation and features creation
    - Data filtering
    - EDA: univariate, bivariate (hypotheses verification) and multivariante analysis (correlation))
    - Data preparation: rescaling and encoding
    - Feature Selection: dataset split, Boruta feature selector
    - Machine Learning modelling: baseline model (average), Linear regression, Random Forest and XGBoost
    - Hyperparamenter fine tunning
    - Convert model performance to bussiness values
    - Deploy: Publish the model at Heroku and criation of a Telegram Bot

### Machine Learning performance 
At this step was used 4 supervisioned algorithms, which were Linear Regression, Lasso Regression, RAndom Forest Regressor and XGBoost Regressor and the average model was used how baseline model.

The train dataset was divide in train and cross validation (CV) to improve the models performance.

|       Model Name          |        MAE CV       |     MAPE CV    |      RMSE CV       |
|:-------------------------:|:-------------------:|:--------------:|:------------------:|
| Baseline Model            |        1353.0003    |   0.2064       |    1835.1355       |                      
| Linear Regression         |  2081.73 +/- 295.63 | 0.30 +/- 0.02  | 2952.53 +/- 468.37 |
| Lasso                     |   2116.4 +/- 341.5  | 0.29 +/- 0.01  | 3057.75 +/- 504.26 |
| Random Forest Regressor   |  837.7 +/- 219.24   | 0.12 +/- 0.02  | 1256.59 +/- 320.28 |
| XGBoost Regressor         |  7049.16 +/- 588.44 | 0.95 +/- 0.0   | 7715.20 +/- 689.21 |

The two best models were Random Forest Regressor and XGBoost. Even though, the metrics of XGboost is worse than Random Forest, it was chosen because it was a performatic model (faster) and the step of hyperparameter tunning brings a great evolution of performance.

To verify the impact of hyperparameter tunning in the performances of models, both models were analysed and the results were showed in the next table.
 
|       Model Name          |        MAE          |     MAPE CV    |      RMSE CV       |
|:-------------------------:|:-------------------:|:--------------:|:------------------:|
|     XGBoost Hypertunned   |        769.41       |   0.11         |    1103.86         |                      
| Random Forest Hypertunned |  686.24             |  0.10          | 1022.09            |

Furthermore, XGBoost was chosen because the model will be hosted on a free cloud, which is a space limitation. Then, in the end it was smaller than Random Forest.

### Business results
After adjust all parameters and to decide what model is better, we have to translate the numbers/metrics/performance in results to CFO, than here was compared the total values of predicitions to best and worst scenario. The results were showed in the next table.

|       Scenario          |        MAE          |
|:-----------------------:|:-------------------:|
|     Predictions         |  R$286,304,320.00   |                      
| Worst scenario          |  R$285,443,146.28   |
| Best scenario           |  R$287,165,461.67   |

### Telegram Bot
The idea of ​​the telegram bot was to provide comfort for the Rossmann CFO to access forecast data from each store wherever he is.

<img src="img/print_telegram.jpeg" alt="drawing" width="500" align="center"/>

### Next steps
In the next steps of cycle:

    - Do a better plots and presents in a dashboard
    - Study more about API


